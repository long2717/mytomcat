# mytomcat

## 项目简介
自己实现的简易的Tomcat

## 实现说明
考虑自己实现一个Tomcat，都有哪些关键的要点呢？

第一，提供 Socket 服务

Tomcat 的启动，必然是 Socket 服务，只不过它支持 HTTP 协议而已！

这里其实可以扩展思考下，Tomcat 既然是基于 Socket，那么是基于BIO or NIO or AIO 呢？

第二，进行请求的分发

要知道一个 Tomcat 可以为多个 Web 应用提供服务，那么很显然，Tomcat 可以把 URL 下发到不同的Web应用。

第三，需要把请求和响应封装成`request / response

我们在 Web 应用这一层，可从来没有封装过 request/response 的，我们都是直接使用的，这就是因为 Tomcat 替我们做好了这一步。

## 代码实现
工程目录结构：

![工程目录结构](resources/pic/工程目录结构.png)


## 必要设置
1. 工程设置
![](resources/pic/工程设置.png)

2. 启动设置
![](resources/pic/启动设置.png)

## 测试

1. 浏览器输入`localhost:8080/girl`并回车

![测试1](resources/pic/测试1.png)

2. 浏览器输入`localhost:8080/world`并回车

![测试2](resources/pic/测试2.png)

3. 控制台显示内容：
```
MyTomcat is Starting...
MyRequest{url='/girl', method='GET'}
MyRequest{url='/world', method='GET'}
```

## TODO
1. 使用解析web.xml的配置文件的方式替换硬编码。
2. 使用NIO替换BIO。

## 参考资料:
1. [从 0 开始手写一个Tomcat，7 步搞定！](https://mp.weixin.qq.com/s/2wioHX9zW7bCdxCMmkszPg)