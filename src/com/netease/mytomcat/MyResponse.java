package com.netease.mytomcat;

import java.io.IOException;
import java.io.OutputStream;

/**
 * 封装响应对象：
 * <p>
 * 基于HTTP协议的格式进行输出写入。
 */
public class MyResponse {
	private OutputStream outputStream;

	public MyResponse(OutputStream outputStream) {
		this.outputStream = outputStream;
	}

	// 将文本转换为字节流
	public void write(String content) throws IOException {
		//HTTP 响应格式
		//HTTP/1.1 200 OK
		//Content-Type:text/html
		//<html>
		//	<head></head>
		//	<body></body>
		//<html>

		StringBuffer httpResponse = new StringBuffer();
		httpResponse.append("HTTP/1.1 200 OK\n")
				.append("Content-Type:text/html\n")
				.append("\r\n")
				.append("<html><head></head><body>")
				.append(content)
				.append("</body></html>");
		outputStream.write(httpResponse.toString().getBytes());
		outputStream.close();
	}

}
