package com.netease.mytomcat;

import java.util.ArrayList;
import java.util.List;

public class ServletMappingConfig {
    public static List<ServeletMapping> serveletMappingList = new ArrayList<>();

    /**
     * 我们在servlet开发中，会在web.xml中通过<servlet></servlet>和<servlet-mapping></servlet-mapping>
     * 来进行指定哪个URL交给哪个servlet进行处理。
     */
    static {
        serveletMappingList.add(new ServeletMapping("findGirl", "/girl", "com.netease.mytomcat.FindGirlServlet"));
        serveletMappingList.add(new ServeletMapping("helloWorld", "/world", "com.netease.mytomcat.HelloWorldServlet"));
    }
}
